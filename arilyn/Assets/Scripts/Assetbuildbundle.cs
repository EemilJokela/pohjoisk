﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Assetbuildbundle: MonoBehaviour
{
	AssetBundle myLoadedAssetBundle;
	public string path;
	public string AssetName;


    void Start()
    {
		LoadAssetBundle(path);
		InstantiateObjectFromBundle(AssetName);
    }

	void LoadAssetBundle(string bundleUrl)
	{
		myLoadedAssetBundle = AssetBundle.LoadFromFile(bundleUrl);

	}

	void InstantiateObjectFromBundle(string assetName)
	{
		var prefab = myLoadedAssetBundle.LoadAsset(assetName);
		Instantiate(prefab);
	}
    
}
